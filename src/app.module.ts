import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { PassengerModule } from './passenger/passenger.module';
import { FlightModule } from './flight/flight.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [".env.development"],
      isGlobal: true,
    }), 
    MongooseModule.forRoot(
      process.env.URI_MONGODB
      , {
        dbName: process.env.DB_NAME,
        // useCreateIndex: true,
        // useFindAndModify: false
    }), UserModule, PassengerModule, FlightModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
