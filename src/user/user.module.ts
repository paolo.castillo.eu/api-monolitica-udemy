import { USER } from './../common/models/models';
import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './schema/user.schema';

@Module({
  controllers: [UserController],
  providers: [UserService],
  imports: [MongooseModule.forFeatureAsync([{
    name: USER.name,
    useFactory:()=>{
      return UserSchema;
    },
  }])]
})
export class UserModule {}
