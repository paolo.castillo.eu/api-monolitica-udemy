# Node nestjs-structure

Node.js framework NestJs project structure

## Instalation

```
$ npm i -g @nestjs/cli
```

## Instalation mongo

```
$ npm i --save mongoose
$ npm i --save mongoose-autopopulate
$ npm i --save @nestjs/mongoose
$ npm i --save @nestjs/config
$ npm i bcrypt
$ npm i class-validator class-transformer
```

## Start project

### Development

```
$ npm run start:dev
```

### Test

```
$ npm test # clude e2e
$ npm run test:e2e
```

### Production

```
$ npm run build
$ npm run start:prod
```

### Command Proyect

> nest --help [nestjs/doc](https://docs.nestjs.com/cli/overview)

````
Usage: nest <command> [options]

Options:
  -v, --version                                   Output the current version.
  -h, --help                                      Output usage information.

Commands:
  new|n [options] [name]                          Generate Nest application.
  build [options] [app]                           Build Nest application.
  start [options] [app]                           Run Nest application.
  info|i                                          Display Nest project details.        
  update|u [options]                              Update Nest dependencies.
  add [options] <library>                         Adds support for an external library 
to your project.
  generate|g [options] <schematic> [name] [path]  Generate a Nest element.
    Schematics available on @nestjs/schematics collection:
      ┌───────────────┬─────────────┬──────────────────────────────────────────────┐   
      │ name          │ alias       │ description                                  │   
      │ application   │ application │ Generate a new application workspace         │   
      │ class         │ cl          │ Generate a new class                         │   
      │ configuration │ config      │ Generate a CLI configuration file            │   
      │ controller    │ co          │ Generate a controller declaration            │   
      │ decorator     │ d           │ Generate a custom decorator                  │   
      │ filter        │ f           │ Generate a filter declaration                │   
      │ gateway       │ ga          │ Generate a gateway declaration               │   
      │ guard         │ gu          │ Generate a guard declaration                 │   
      │ interceptor   │ in          │ Generate an interceptor declaration          │   
      │ interface     │ interface   │ Generate an interface                        │   
      │ module        │ mo          │ Generate a module declaration                │   
      │ pipe          │ pi          │ Generate a pipe declaration                  │   
      │ provider      │ pr          │ Generate a provider declaration              │   
      │ resolver      │ r           │ Generate a GraphQL resolver declaration      │   
      │ service       │ s           │ Generate a service declaration               │   
      │ sub-app       │ app         │ Generate a new application within a monorepo │   
      │ resource      │ res         │ Generate a new CRUD resource                 │   
      └───────────────┴─────────────┴──────────────────────────────────────────────┘
``` 
